﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DaneZPlikuConsole
{
    class Program
    {
        static string TablicaDoString<T>(T[][] tab)
        {
            string wynik = "";
            for (int i = 0; i < tab.Length; i++)
            {
                for (int j = 0; j < tab[i].Length; j++)
                {
                    wynik += tab[i][j].ToString() + "\t";
                }
                wynik = wynik.Trim() + Environment.NewLine;
            }

            return wynik;
        }

        static double StringToDouble(string liczba)
        {
            double wynik; liczba = liczba.Trim();
            if (!double.TryParse(liczba.Replace(',', '.'), out wynik) && !double.TryParse(liczba.Replace('.', ','), out wynik))
                throw new Exception("Nie udało się skonwertować liczby do double");

            return wynik;
        }


        static int StringToInt(string liczba)
        {
            int wynik;
            if (!int.TryParse(liczba.Trim(), out wynik))
                throw new Exception("Nie udało się skonwertować liczby do int");

            return wynik;
        }

        static string[][] StringToTablica(string sciezkaDoPliku)
        {
            string trescPliku = System.IO.File.ReadAllText(sciezkaDoPliku); // wczytujemy treść pliku do zmiennej
            string[] wiersze = trescPliku.Trim().Split(new char[] { '\n' }); // treść pliku dzielimy wg znaku końca linii, dzięki czemu otrzymamy każdy wiersz w oddzielnej komórce tablicy
            string[][] wczytaneDane = new string[wiersze.Length][];   // Tworzymy zmienną, która będzie przechowywała wczytane dane. Tablica będzie miała tyle wierszy ile wierszy było z wczytanego poliku

            for (int i = 0; i < wiersze.Length; i++)
            {
                string wiersz = wiersze[i].Trim();     // przypisuję i-ty element tablicy do zmiennej wiersz
                string[] cyfry = wiersz.Split(new char[] { ' ' });   // dzielimy wiersz po znaku spacji, dzięki czemu otrzymamy tablicę cyfry, w której każda oddzielna komórka to czyfra z wiersza
                wczytaneDane[i] = new string[cyfry.Length];    // Do tablicy w której będą dane finalne dokładamy wiersz w postaci tablicy integerów tak długą jak długa jest tablica cyfry, czyli tyle ile było cyfr w jednym wierszu
                for (int j = 0; j < cyfry.Length; j++)
                {
                    string cyfra = cyfry[j].Trim(); // przypisuję j-tą cyfrę do zmiennej cyfra
                    wczytaneDane[i][j] = cyfra; 
                }
            }
            return wczytaneDane;
        }
        static double sredniaFunkcja(List<double> liczby)
        {
            double suma = 0;
            for(int i=0; i < liczby.Count(); i++)
            {
                suma += liczby[i];
            }

            return suma / liczby.Count();
        }


        static double odchylenieStandardowe(List<double> liczby)
        {
            double result = 0, srednia;

            srednia = sredniaFunkcja(liczby);

            for (int i = 0; i < liczby.Count(); i++)
            {
                result += (liczby[i] - srednia) * (liczby[i] - srednia);
            }
            result = Math.Sqrt(result / (liczby.Count() - 1
                ));


            return result;
        }

        static double normalizacja(double x, double in_min, double in_max, double out_min, double out_max)
        {
            return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
        }

        static Random random = new Random();
        public static List<int> GenerateRandom(int count)
        {
            // generate count random values.
            HashSet<int> candidates = new HashSet<int>();
            while (candidates.Count < count)
            {
                // May strike a duplicate.
                candidates.Add(random.Next());
            }

            // load them in to a list.
            List<int> result = new List<int>();
            result.AddRange(candidates);

            // shuffle the results:
            int i = result.Count;
            while (i > 1)
            {
                i--;
                int k = random.Next(i + 1);
                int value = result[k];
                result[k] = result[i];
                result[i] = value;
            }
            return result;
        }

        static void Main(string[] args)
        {
            string nazwaPlikuZDanymi = @"australian.txt";
            string nazwaPlikuZTypamiAtrybutow = @"australian-type.txt";

            string[][] wczytaneDane = StringToTablica(nazwaPlikuZDanymi);
            string[][] atrType = StringToTablica(nazwaPlikuZTypamiAtrybutow);


            Console.WriteLine("Dane systemu");

            string nazwyKolumn = "";
            string typyKolumn = "";
            for(int i=0; i <atrType.Length; i++)
            {
                nazwyKolumn += atrType[i][0] +"\t";
                typyKolumn += atrType[i][1] +"\t";
            }
            nazwyKolumn += "decyz";
            typyKolumn += "decyz";
            Console.WriteLine(nazwyKolumn);
            Console.WriteLine(typyKolumn+"\n");


            string wynik = TablicaDoString(wczytaneDane);
            Console.Write(wynik);

            Console.WriteLine("");
            Console.WriteLine("Dane pliku z typami");

            string wynikAtrType = TablicaDoString(atrType);
            Console.Write(wynikAtrType);


            /****************** Miejsce na rozwiązanie *********************************/

            


            //Console.WriteLine("\n\n Zadanie 3");


            Console.WriteLine("-----3a) symbole klas decyzyjnych: ");




            List<string> symboleKlasDecyzyjnych = new List<string>();

            for (int i = 0; i < wczytaneDane.Length; i++)
            {
                symboleKlasDecyzyjnych.Add(wczytaneDane[i][wczytaneDane[i].Length - 1]);
            }


            int counter = 1;
            foreach (string element in symboleKlasDecyzyjnych.Distinct())
            {
                Console.WriteLine(counter+":\t"+ element);
                counter += 1;
            }
            Console.WriteLine("-----3a)----------------------------end--\n");


            Console.WriteLine("-----3b) wielkosc klas decyzyjnych: ");


            
            foreach (string element in symboleKlasDecyzyjnych.Distinct())
            {
                int elementCounter = 0;
                for (int i = 0; i < wczytaneDane.Length; i++)
                {
                    //Console.WriteLine( wczytaneDane[0].Length);
                    if (wczytaneDane[i][wczytaneDane[0].Length - 1] == element)
                        elementCounter += 1;

                }
                Console.WriteLine(element+":\t"+ elementCounter);


            }

            Console.WriteLine("-----3b)----------------------------end--\n");



            Console.WriteLine("-----3c) min i max wartosci: ");

            
            for (int i = 0; i < atrType.Length; i++)
            {
                //Console.WriteLine(atrType[i][1]);

                if(atrType[i][1] == "n")
                {
                    List<double> row2 = new List<double>();
                    for (int j=0; j < wczytaneDane.Length; j++)
                    {
                        row2.Add(StringToDouble(wczytaneDane[j][i]));
                    }
                    Console.WriteLine("min dla \t" + atrType[i][0]+" = "+ row2.Min());
                    Console.WriteLine("max dla \t" + atrType[i][0]+" = "+ row2.Max());
                }
            }



            Console.WriteLine("-----3c)----------------------------end--\n");


            Console.WriteLine("-----d) ilosc roznych wartosci atrybutow: ");

            for (int i = 0; i < atrType.Length; i++)
            {
                //Console.WriteLine(atrType[i][0]);

                List<string> oneColumn = new List<string>();
                for (int j = 0; j < wczytaneDane.Length; j++)
                {
                    oneColumn.Add(wczytaneDane[j][i]);
                }
                Console.WriteLine(atrType[i][0]+":\t"+ oneColumn.Distinct().Count());
                


            }


            Console.WriteLine("-----3d)----------------------------end--\n");


            Console.WriteLine("-----3e) rozne wartosci atrybutow: ");


            for (int i = 0; i < atrType.Length; i++)
            {
                //Console.WriteLine(atrType[i][0]);

                List<string> oneColumn = new List<string>();
                for (int j = 0; j < wczytaneDane.Length; j++)
                {
                    oneColumn.Add(wczytaneDane[j][i]);
                }
                Console.WriteLine("\t"+atrType[i][0] + ":\t----------------------------\t" + oneColumn.Distinct().Count()+" elementow");
                foreach(string element in oneColumn.Distinct())
                {
                    Console.WriteLine(element);
                }



            }


            Console.WriteLine("-----3e)----------------------------end--\n");



            Console.WriteLine("-----3f) odchylenie: ");

            for (int i = 0; i < atrType.Length; i++)
            {
                //Console.WriteLine(atrType[i][1]);

                if (atrType[i][1] == "n")
                {
                    List<double> row2 = new List<double>();
                    for (int j = 0; j < wczytaneDane.Length; j++)
                    {
                        row2.Add(StringToDouble(wczytaneDane[j][i]));
                    }
                    Console.WriteLine("odchylenie dla \t" + atrType[i][0] + " = " + odchylenieStandardowe(row2));
                    
                }
            }

            Console.WriteLine("-----3f)----------------------------end--\n");



            Console.WriteLine("-----4a) generowanie: ");


            int iloscELementow = 0;
            iloscELementow = wczytaneDane.Length * (wczytaneDane[0].Length - 1);
            Console.WriteLine(iloscELementow);

            int counter2 = 0;
            for (int i = 0; i < atrType.Length; i++)
            {
                //Console.WriteLine(atrType[i][1]);

                if (atrType[i][1] == "n")
                {
                    for (int j = 0; j < wczytaneDane.Length; j++)
                    {
                        counter2 += 1;
                    }
                    
                   
                }
            }
            double ileProcent = 10;
            int ileElementowWylosowac =  (int)(counter2 * (ileProcent / 100));
            
            Console.WriteLine("ilosc elementow numerycznych: \t" + counter2+"*"+ileProcent+"="+ileElementowWylosowac);


            string[][] wczytaneDaneZad4a = wczytaneDane;
            string[][] atrTypeZad4 = atrType;

            Console.WriteLine("Dane systemu");

            string nazwyKolumn2 = "";
            string typyKolumn2 = "";
            for (int i = 0; i < atrTypeZad4 .Length; i++)
            {
                nazwyKolumn2 += atrTypeZad4[i][0] + "\t";
                typyKolumn2 += atrTypeZad4[i][1] + "\t";
            }
            nazwyKolumn2 += "decyz";
            typyKolumn2 += "decyz";
            Console.WriteLine(nazwyKolumn2);
            Console.WriteLine(typyKolumn2 + "\n");


            for (int i = 0; i < ileElementowWylosowac; i++)
            {
                Random rndi = new Random();
                Random rndj = new Random();
                wczytaneDaneZad4a[rndi.Next(0, wczytaneDaneZad4a.Length)][rndj.Next(0, wczytaneDaneZad4a[0].Length-1)] = "?";
            }




            string wynika = TablicaDoString(wczytaneDaneZad4a);
            Console.Write(wynika);










            Console.WriteLine("-----4a)----------------------------end--\n");

            Console.WriteLine("-----4b) normalizacja: ");

            string[][] wczytaneDaneZad4b = wczytaneDane;

            for (int i = 0; i < atrType.Length; i++)
            {
                //Console.WriteLine(atrType[i][1]);

                if (atrType[i][1] == "n")
                {
                    List<double> row2 = new List<double>();
                    for (int j = 0; j < wczytaneDane.Length; j++)
                    {
                        row2.Add(StringToDouble(wczytaneDaneZad4b[j][i]));
                    }

                    //konwersja do <-10,10>

                    for (int j = 0; j < wczytaneDane.Length; j++)
                    {
                        wczytaneDaneZad4b[j][i] = normalizacja(StringToDouble(wczytaneDaneZad4b[j][i]), row2.Min(), row2.Max(), -10, 10).ToString();
                    }
                }
            }
            string wynikb = TablicaDoString(wczytaneDaneZad4b);
            //Console.Write(wynikb);

            Console.WriteLine("-----4b)----------------------------end--\n");


            Console.WriteLine("-----4c) standaryzacja: ");


            string[][] wczytaneDaneZad4c = wczytaneDane;

            for (int i = 0; i < atrType.Length; i++)
            {
                //Console.WriteLine(atrType[i][1]);

                if (atrType[i][1] == "n")
                {
                    List<double> row2 = new List<double>();
                    for (int j = 0; j < wczytaneDane.Length; j++)
                    {
                        row2.Add(StringToDouble(wczytaneDaneZad4c[j][i]));
                    }
                    double srednia = sredniaFunkcja(row2);
                    double wariancja = odchylenieStandardowe(row2)* odchylenieStandardowe(row2);

                    for (int j = 0; j < wczytaneDane.Length; j++)
                    {
                        //wczytaneDaneZad4c[j][i] = ((StringToDouble(wczytaneDaneZad4c[j][i])-srednia)/wariancja).ToString();
                    }
                }
            }

            string wynikc = TablicaDoString(wczytaneDaneZad4c);
            Console.Write(wynikc);

            Console.WriteLine("-----4c)----------------------------end--\n");


            /****************** Koniec miejsca na rozwiązanie ********************************/
            Console.ReadKey();
        }
    }
}
