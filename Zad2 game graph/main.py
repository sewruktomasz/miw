import gvgen  # this library helps to create Graphviz graphs
#downloaded from https://github.com/stricaud/gvgen

#Graph class
class Graph:
    def __init__(self):
        self.core = None

    def createGraph(self):
        if self.core != None:
            return

        core = Logic(None, 0, 0)
        core.createSubgraph()

        self.core = core
        return

    def showGraph(self):
        self.core.display()

        return

    def chooseBestOption(self):
        self.result = self.core.chooseBestOption()

        return

    def getGraph(self):
        graph = gvgen.GvGen()

        if self.core == None:
            return

        self.core.addToGraph(graph)

        return graph



#here is a game logic
class Logic:
    def __init__(self, previousGraph, pointsCounter, movesCounter):
        self.gameLogicPossibleThrows = [4, 5, 6]
        self.gameLogicPossibleResult = 21

        self.previousGraph = previousGraph
        self.pointsCounter = pointsCounter
        self.movesCounter = movesCounter

        self.subgraphThrow4 = None
        self.subgraphThrow5 = None
        self.subgraphThrow6 = None

        self.graphDescription = None
        self.result = None
        self.choosen = None



    def __str__(self):
        return str(self.pointsCounter)

    def chooseBestOption(self):
        if self.finishBuildingGraph():
            return self.getEndResult()

        if self.isProtagonist():
            child = self.getChildWithHighestResult()
        else:
            child = self.getChildWithLowestResult()

        child.chooseBest(True)

        self.setEndResult(child.getEndResult())

        return child.getEndResult()

    def getChildrenList(self):
        return ([self.subgraphThrow4, self.subgraphThrow5, self.subgraphThrow6])

    def getChildrenEndResults(self):
        leftResult = self.subgraphThrow4.chooseBestOption()
        middleResult = self.subgraphThrow5.chooseBestOption()
        rightResult = self.subgraphThrow6.chooseBestOption()

        return ([leftResult, middleResult, rightResult])

    def getChildWithHighestResult(self):
        maxResult = max(self.getChildrenEndResults())

        for child in self.getChildrenList():
            if child.getEndResult() == maxResult:
                return child

    def getChildWithLowestResult(self):
        minResult = min(self.getChildrenEndResults())

        for child in self.getChildrenList():
            if child.getEndResult() == minResult:
                return child


    def addToGraph(self, graph, parentGraphNode=None):
        label = self.getLabel()

        self.graphDescription = graph.newItem(label)

        if not self.isCoreGraph() and parentGraphNode != None:
            nodeLink = graph.newLink(parentGraphNode, self.graphDescription)
            graph.propertyAppend(nodeLink, 'label', self.pointsCounter - int(str(self.previousGraph)))


            if self.choosen == True:
                if (self.previousGraph.isProtagonist()):
                    graph.propertyAppend(nodeLink, 'color', '#f03434')

        if self.finishBuildingGraph():
            return

        self.subgraphThrow4.addToGraph(graph, self.graphDescription)
        self.subgraphThrow5.addToGraph(graph, self.graphDescription)
        self.subgraphThrow6.addToGraph(graph, self.graphDescription)

        return

    def calculateEndResultByValue(self):
        if self.winner():
            return 1

        if self.looser():
            return -1

    def getEndResult(self):
        return self.result

    def setEndResult(self, result):
        self.result = result
        return

    def chooseBest(self, selected):
        self.choosen = selected

        return

    def display(self):

        # if is end node (has no children) exit
        if self.finishBuildingGraph():
            return

        self.subgraphThrow4.display()
        self.subgraphThrow5.display()
        self.subgraphThrow6.display()

        return

    def getLabel(self):
        if self.finishBuildingGraph():
            return self.getEndNodeLabel()

        if self.isProtagonist() == True:
            return 'prot\\n' + str(self.pointsCounter)
        return 'ant\\n' + str(self.pointsCounter)

    def getEndNodeLabel(self):
        if self.isProtagonist() == True:
            return 'prot\\n' + str(self.pointsCounter)
        return 'ant\\n' + str(self.pointsCounter)

    def isCoreGraph(self):
        return self.movesCounter == 0

    def finishBuildingGraph(self):
        return self.subgraphThrow4 == None and self.subgraphThrow5 == None and self.subgraphThrow6 == None

    def isProtagonist(self):
        return self.movesCounter % 2 == 0

    def winner(self):
        return self.finishBuildingGraph() and self.pointsCounter == self.gameLogicPossibleResult

    def looser(self):
        return self.finishBuildingGraph() and self.pointsCounter > self.gameLogicPossibleResult

    def createSubgraph(self):
        if self.pointsCounter >= 21:
            pointsCounter = self.calculateEndResultByValue()
            self.setEndResult(pointsCounter)
            return

        self.addSubgraphThrow4()
        self.subgraphThrow4.createSubgraph()

        self.addSubgraphThrow5()
        self.subgraphThrow5.createSubgraph()

        self.addSubgraphThrow6()
        self.subgraphThrow6.createSubgraph()

        return

    def addSubgraphThrow4(self):
        subgraphMovesCounter = self.movesCounter + 1
        subgraphPointsCounter = self.pointsCounter + self.gameLogicPossibleThrows[0]

        self.subgraphThrow4 = Logic(self, subgraphPointsCounter, subgraphMovesCounter)

        return

    def addSubgraphThrow5(self):
        subgraphMovesCounter = self.movesCounter + 1
        subgraphPointsCounter = self.pointsCounter + self.gameLogicPossibleThrows[1]

        self.subgraphThrow5 = Logic(self, subgraphPointsCounter, subgraphMovesCounter)

        return

    def addSubgraphThrow6(self):
        subgraphMovesCounter = self.movesCounter + 1
        subgraphPointsCounter = self.pointsCounter + self.gameLogicPossibleThrows[2]

        self.subgraphThrow6 = Logic(self, subgraphPointsCounter, subgraphMovesCounter)

        return


myGraph = Graph()
myGraph.createGraph()
myGraph.chooseBestOption()

sep = ""
for i in range(100):
    sep += "-"
print(sep)

graph = myGraph.getGraph()
graph.dot()
