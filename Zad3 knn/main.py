from math import sqrt

k = 3  # k - parameter


def fileToArray(fileName):
    file = open(fileName, "r")

    contentArrayTmp = []
    for line in file:
        contentArrayTmp.append(line.split("\t"))

    contentArray = []
    for row in contentArrayTmp:
        tmpRow = []
        for element in row[:-1]:
            tmpRow.append(float(element))
        tmpRow.append(str(int(row[-1])))

        contentArray.append(tmpRow)

    # sepal_length_in_cm    n
    # sepal_width_in_cm    n
    # petal_length_in_cm    n
    # petal_width_in_cm    n
    # class(1=Setosa, 2=Versicolour, 3=Virginica)    s
    return contentArray


def minOfCol(data, colNo=0):
    result = []
    for row in data:
        result.append(row[colNo])
    return min(result)


def maxOfCol(data, colNo=0):
    result = []
    for row in data:
        result.append(row[colNo])
    return max(result)


# def minAndMaxOfData(data):
#     result = []
#     quantityOfParam = len(data[0]) - 1  # -1 because last is class col
#
#     for i in range(quantityOfParam):
#         for row in data:
#             result.append(row[i])
#
#     print(result)


def normalization(data):
    result = []
    for row in data:
        tmpRow = []
        for element in row[:-1]:
            elementIndex = row.index(element)
            tmpRow.append(round((element - minOfCol(data, elementIndex)) / (
                    maxOfCol(data, elementIndex) - minOfCol(data, elementIndex)), 1))
        tmpRow.append(row[-1])
        result.append(tmpRow)

    return result


# euklides metrics
def metricsEuklidesClassName(data, testRowData=[1, 2, 3, 4], k=3):
    result = []
    quantityOfParameters = len(data[0]) - 1  # -1 because last is class column, so we dont need this

    for row in data:
        tmpSum = 0
        for i in range(quantityOfParameters):
            count = (row[i] - testRowData[i]) ** 2

            # tmpCol.append(count)
            tmpSum += count
        result.append([sqrt(tmpSum), row[-1]])

    return returnClassName(result, k)


def dataEuklides(data, k=3):
    result = data
    for i in range(len(data)):
        result[i][-1] = metricsEuklidesClassName(data[:i] + data[i + 1:], data[i], k)
        # print(metricsEuklidesClassName(data[:i] + data[i + 1:], data[i], k))
    return result


# manhattan metrics
def metricsManhattanClassName(data, testRowData=[1, 2, 3, 4], k=3):
    result = []
    quantityOfParameters = len(data[0]) - 1  # -1 because last is class column, so we dont need this

    for row in data:
        tmpSum = 0
        for i in range(quantityOfParameters):
            count = abs(row[i] - testRowData[i])

            # tmpCol.append(count)
            tmpSum += count
        result.append([tmpSum, row[-1]])

    return returnClassName(result, k)


def dataManhattan(data, k=3):
    result = data
    for i in range(len(data)):
        result[i][-1] = metricsManhattanClassName(data[:i] + data[i + 1:], data[i], k)
        # print(metricsEuklidesClassName(data[:i] + data[i + 1:], data[i], k))
    return result


# Minkowski metrics
def metricsMinkowskiClassName(data, testRowData=[1, 2, 3, 4], k=3, p=2):
    result = []
    quantityOfParameters = len(data[0]) - 1  # -1 because last is class column, so we dont need this

    for row in data:
        tmpSum = 0
        for i in range(quantityOfParameters):
            count = abs(row[i] - testRowData[i]) ** p

            # tmpCol.append(count)
            tmpSum += count
        result.append([(tmpSum) ** (1 / p), row[-1]])

    return returnClassName(result, k)


def dataMinkowski(data, k=3, p=2):
    result = data
    for i in range(len(data)):
        result[i][-1] = metricsMinkowskiClassName(data[:i] + data[i + 1:], data[i], k, p)
        # print(metricsEuklidesClassName(data[:i] + data[i + 1:], data[i], k))
    return result


def returnClassName(classesDict, k=3):
    classesDict = divideIntoClasses(classesDict)
    classesSum = []

    for classNo in ['1', '2', '3']:
        tmpSum = 0
        for i in range(k):
            tmpSum += classesDict[classNo][i]
        classesSum.append(tmpSum)

    # print(classesSum)

    if (classesSum[0] < classesSum[1] and classesSum[0] < classesSum[2]):
        return '1'  # class number
    elif (classesSum[1] < classesSum[0] and classesSum[1] < classesSum[2]):
        return '2'
    elif (classesSum[2] < classesSum[0] and classesSum[2] < classesSum[1]):
        return '3'
    else:
        return 'Same value for at least 2 classes'


def divideIntoClasses(data):  # e.g. data = [[5.2, '1'], [4.3, '3'], [4.5, '2']]
    class1_Setosa = []  # Setosa
    class2_Versicolour = []  # Versicolour
    class3_Virginica = []  # Virginica

    for row in data:
        if (row[-1] == '1'):
            class1_Setosa.append(row[0])
        elif (row[-1] == '2'):
            class2_Versicolour.append(row[0])
        elif (row[-1] == '3'):
            class3_Virginica.append(row[0])
        else:
            return "Weird class name"

    class1_Setosa.sort()
    class2_Versicolour.sort()
    class3_Virginica.sort()

    return {
        '1': class1_Setosa,
        '2': class2_Versicolour,
        '3': class3_Virginica
    }


def countClassCorrectness(originalData, convertedData):
    quantityOfRows = len(originalData)
    quantityOfDifference = 0
    for i in range(len(originalData)):
        if (originalData[i][-1] != convertedData[i][-1]):
            quantityOfDifference += 1
    # print(originalData)
    # print(convertedData)
    return (quantityOfRows - quantityOfDifference) / quantityOfRows * 100


# --------------------------------------------------

data = fileToArray("iris.txt")
normalizedData = normalization(data)

print("------------Ex1 Data from file--------------")
print(['n', 'n', 'n', 'n', 's'], "\n")
for row in data:
    print(row)
print("FINISHED------------Data from file--------------\n\n\n")
# print(normalization(data))
# print(minAndMaxOfData(data))


print("------------Ex2 Normalized file data--------------")
print(['n', 'n', 'n', 'n', 's'], "\n")
for row in normalizedData:
    print(row)
print("FINISHED------------Normalized file data--------------\n\n\n")

print("------------Ex3 Converted data by Euklides metrics--------------")
print(['n', 'n', 'n', 'n', 's'], "\n")

convertedData = dataEuklides(normalizedData, k=5)  # here You can edit k-parameter

for row in convertedData:
    print(row)

normalizedData = normalization(data)
print("Ex4 Correctness of clasification (Euklides metrics):",
      round(countClassCorrectness(normalizedData, convertedData), 2), "%")

print("FINISHED------------Converted data by Euklides metrics--------------\n\n\n")

print("------------Ex3 Converted data by Manhattam metrics--------------")
print(['n', 'n', 'n', 'n', 's'], "\n")

convertedData = dataManhattan(normalizedData, k=5)  # here You can edit k-parameter

for row in convertedData:
    print(row)

normalizedData = normalization(data)
print("Ex4 Correctness of clasification (Manhattan metrics):",
      round(countClassCorrectness(normalizedData, convertedData), 2), "%")

print("FINISHED------------Converted data by Manhattam metrics--------------\n\n\n")

print("------------Ex3 Converted data by Minkowski metrics--------------")
print(['n', 'n', 'n', 'n', 's'], "\n")

convertedData = dataMinkowski(normalizedData, k=5, p=1)  # here You can edit k-parameter and p-parameter

for row in convertedData:
    print(row)

normalizedData = normalization(data)
print("Ex4 Correctness of clasification (Minkowski metrics):",
      round(countClassCorrectness(normalizedData, convertedData), 2), "%")

print("FINISHED------------Converted data by Minkowski metrics--------------\n\n\n")

while (True):
    metricsType = int(input("Insert metrics nuber (1-euklides, 2-Manhattan, 3-Minkowski:  "))
    kParam = int(input("Insert k-parameter (e.g. 3):  "))

    if (metricsType == 1):
        print("------------Converted data by Euklides metrics--------------")


        convertedData = dataEuklides(normalizedData, kParam)

        for row in convertedData:
            print(row)

        normalizedData = normalization(data)
        print("Correctness of clasification (Euklides metrics)(k-parameter =",kParam,")",
              round(countClassCorrectness(normalizedData, convertedData), 2), "%")

        print("FINISHED------------Converted data by Euklides metrics--------------\n\n\n")


    if (metricsType == 2):
        print("------------Converted data by Euklides metrics--------------")


        convertedData = dataManhattan(normalizedData, kParam)

        for row in convertedData:
            print(row)

        normalizedData = normalization(data)
        print("Correctness of clasification (Manhattan metrics)(k-parameter =",kParam,")",
              round(countClassCorrectness(normalizedData, convertedData), 2), "%")

        print("FINISHED------------Converted data by Euklides metrics--------------\n\n\n")

    if (metricsType == 3):
        pParam = float(input("Insert p-parameter:  "))
        print("------------Converted data by Euklides metrics--------------")

        convertedData = dataMinkowski(normalizedData, kParam, pParam)

        for row in convertedData:
            print(row)

        normalizedData = normalization(data)
        print("Correctness of clasification (Euklides metrics)(k-parameter =", kParam, ") (p-parameter =",pParam,")",
              round(countClassCorrectness(normalizedData, convertedData), 2), "%")

        print("FINISHED------------Converted data by Euklides metrics--------------\n\n\n")
    else:
        print("Try again")