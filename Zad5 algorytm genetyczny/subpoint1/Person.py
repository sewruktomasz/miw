from math import sin,cos

class Person:
    def __init__(self, param1, param2, genotype):
        self.x1 = param1
        self.x2 = param2
        self.genotype = genotype

    def formulaScore(self):
        return round( sin(self.x1 * 0.05) + sin(self.x2 * 0.05) + 0.4 * sin(self.x1 * 0.15) * sin(self.x2 * 0.15), 10)

